import { Component, OnInit } from '@angular/core';
import { Shoe } from '../modeliEntiteta/shoe.model';
import { Category } from '../modeliEntiteta/category.model';
import { Brand } from '../modeliEntiteta/brand.model';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {
  public shoes: Shoe[]=[];
  public newShoe: Shoe;
  public newShoeUpdate: Shoe;
  public categories: Category[]=[];
  public brands: Brand[]=[];
  public clickedUpdate:boolean=false;

  public orderTypes = Order;
  public priceFilter: PriceLimits;

   /*
   public brandExists:boolean=false;
   public categoryExists:boolean=false
  */

  constructor(private http: Http, private httpClient:HttpClient,private route: ActivatedRoute) {
    //this.initData();
    this.priceFilter = {
      lowest:0,
      highest: 0
    }
    this.newShoe=emptyShoe();
    this.newShoeUpdate=emptyShoe();
    this.loadData();
  }
  

  ngOnInit() {
  }

  resetNewShoe() {
    this.newShoe = emptyShoe();
  }


 //METODE ZA KOMUNIKACIJU SA SERVEROM:

  private loadData(order?: Order){
    let params: URLSearchParams = new URLSearchParams();
    params.set('lowestPrice', this.priceFilter.lowest.toString());
    params.set('highestPrice', this.priceFilter.highest.toString());
    if(order !== undefined){
      params.set('ord', order.toString());
    }
    let options = new RequestOptions({ search: params });
    this.http.get('/api/shoes',options).subscribe(
        (res: Response) => {
          this.shoes=res.json();
        }
    );
    
    this.loadBrands();
    this.loadCategories();
   
  }


  save(){
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.httpClient.post('/api/shoes', JSON.stringify(this.newShoe), {headers}).subscribe((resp)=>{
      this.loadData();
      this.resetNewShoe();
    });
  };

  deleteShoe(id:number){
    this.http.delete('/api/shoe/'+id).subscribe(
      (res: Response) => {
        this.loadData();
      }
    );
  }

  findShoeById(id:number){
    this.http.get('/api/shoes/'+id).subscribe(
      (res: Response) => {
        this.newShoeUpdate=res.json();
        this.clickedUpdate=true;
      }
    );
  }


  updateInfo(){
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    this.http.put(`api/shoe/${this.newShoeUpdate.id}`, JSON.stringify(this.newShoeUpdate), options).subscribe(
      (res: Response) => {
      
        this.clickedUpdate=false;
        this.newShoeUpdate=emptyShoe();
        this.loadData();
      }
    )
  }

  filterByPrice (){
     this.loadData();
  }
  
  
  resetFilter(){
    this.priceFilter = {
      lowest:0,
      highest: 0
    }
    this.loadData();
  }
  
  loadBrands(){
    this.http.get('/api/brands').subscribe(
      (res: Response) => {
        this.brands=res.json();
      }
    );
  }

  loadCategories(){
    this.http.get('/api/categories').subscribe(
      (res: Response) => {
        this.categories=res.json();
      }
    );
  }
  

/*
  findCategoryByName(){
      let queryParams = new URLSearchParams();
      queryParams.set('categoryName', this.newShoe.category.categoryName);
      let options = new RequestOptions({ search: queryParams });
      this.http.get('/api/categories',options).subscribe(
        (res: Response) => {
          this.newShoe.category=res.json();
        }
    );
  }

  findBrandByName(){
      let queryParams = new URLSearchParams();
      queryParams.set('brandName', this.newShoe.brand.brandName);
      let options = new RequestOptions({ search: queryParams });
      this.http.get('/api/brands',options).subscribe(
        (res: Response) => {
          this.newShoe.brand=res.json();
        }
    );
  }
}
*/  
}//end class
  



export enum Order{
  asc,
  desc
}


interface PriceLimits{
  lowest: number;
  highest: number;
}

 

  const emptyShoe = () => {
    return new Shoe({
      id:0,
	    title: '',
	    imageURL: '',
	    price: 0,
	    category:new Category({
        id:0,
        categoryName:''
      }),
  	  brand:new Brand({
        id:0,
        brandName: ''
      })
    });
  }

  
  const emptyCategory = () => {
    return new Category({
      id:0,
      categoryName: ''
    })
  };
  
  const emptyBrand = () => {
    return new Brand({
      id:0,
      brandName: ''
    })
  };

