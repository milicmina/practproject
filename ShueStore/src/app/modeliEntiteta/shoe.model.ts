import { Category } from './category.model';
import { Brand } from './brand.model';


export class Shoe implements ShoeInterface{

	public id:number;
	public title: string;
	public imageURL: string;
	public price: number;
	public category: Category;
	public brand: Brand;

		
	constructor(shoeCfg:ShoeInterface){
		this.id=shoeCfg.id;
		this.title=shoeCfg.title;
		this.imageURL=shoeCfg.imageURL;
		this.price=shoeCfg.price;
		this.category=shoeCfg.category;
		this.brand=shoeCfg.brand;
	}


}


interface ShoeInterface{
	id:number;
	title: string;
	imageURL: string;
	price: number;
	category: Category;
	brand: Brand;
}