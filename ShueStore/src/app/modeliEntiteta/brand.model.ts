export class Brand implements BrandInterface {

    public id: number;
    public brandName: string;

    constructor(brandCfg: BrandInterface) {
        this.id = brandCfg.id;
        this.brandName=brandCfg.brandName;
    }
}

interface BrandInterface {
   id: number;
   brandName: string;
}