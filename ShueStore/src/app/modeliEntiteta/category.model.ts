export class Category implements CategoryInterface {

    public id: number;
    public categoryName: string;

    constructor(categoryCfg: CategoryInterface) {
        this.id = categoryCfg.id;
        this.categoryName=categoryCfg.categoryName;
    }
}

interface CategoryInterface {
   id: number;
   categoryName: string;
}