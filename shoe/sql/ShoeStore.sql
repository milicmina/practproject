create database shoeStore;

use shoeStore;

insert into brand (brand_name) values ('Nike');
insert into brand (brand_name) values ('Belstaff');
insert into brand (brand_name) values ('Clarks');

insert into category (category_name) values ('Boots');
insert into category (category_name) values ('Sandals');
insert into category (category_name) values ('Sneakers');

insert into shoe (imageurl, price, title, brand_id, category_id) values 
('https://upload.wikimedia.org/wikipedia/commons/d/dc/74892143_f94145facb.jpg', 7500,'BlueVision', 1,3);
insert into shoe (imageurl, price, title, brand_id, category_id) values 
('https://upload.wikimedia.org/wikipedia/commons/c/c4/BootsBr.jpg', 28000,'ElegantStyle', 2,1);
insert into shoe (imageurl, price, title, brand_id, category_id) values 
('https://upload.wikimedia.org/wikipedia/commons/9/93/Irvine_Jane_Doe_shoe.jpg', 15000,'UrbanLegend', 3,2);

select * from brand;
select * from category;
select * from shoe;

show columns from shoe;