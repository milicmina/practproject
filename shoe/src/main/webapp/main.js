(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _main_component_main_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main-component/main-component.component */ "./src/app/main-component/main-component.component.ts");
/* harmony import */ var _page_not_found_component_page_not_found_component_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-not-found-component/page-not-found-component.component */ "./src/app/page-not-found-component/page-not-found-component.component.ts");
/* harmony import */ var _main_component_backend_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main-component/backend.service */ "./src/app/main-component/backend.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    { path: 'main', component: _main_component_main_component_component__WEBPACK_IMPORTED_MODULE_7__["MainComponentComponent"] },
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: '**', component: _page_not_found_component_page_not_found_component_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponentComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _main_component_main_component_component__WEBPACK_IMPORTED_MODULE_7__["MainComponentComponent"],
                _page_not_found_component_page_not_found_component_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponentComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(appRoutes, { enableTracing: true } // <-- debugging purposes only
                )
            ],
            providers: [
                _main_component_backend_service__WEBPACK_IMPORTED_MODULE_9__["BackendService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/main-component/backend.service.ts":
/*!***************************************************!*\
  !*** ./src/app/main-component/backend.service.ts ***!
  \***************************************************/
/*! exports provided: BackendService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackendService", function() { return BackendService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackendService = /** @class */ (function () {
    function BackendService() {
    }
    BackendService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], BackendService);
    return BackendService;
}());



/***/ }),

/***/ "./src/app/main-component/main-component.component.css":
/*!*************************************************************!*\
  !*** ./src/app/main-component/main-component.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/main-component/main-component.component.html":
/*!**************************************************************!*\
  !*** ./src/app/main-component/main-component.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align:center\">\r\n  <h1 style=\"color:saddlebrown;font-size: x-large; font-weight: bolder\">\r\n    Welcome to Shoe Store!\r\n  </h1>\r\n</div>\r\n<div style=\"background-color: rgb(173, 192, 155); width:40% ;float:left\">\r\n  <ul>\r\n      <li *ngFor=\"let shoe of shoes; let i = index;\">\r\n          \r\n              <div>\r\n                  <div>Title: {{shoe.title}}</div>\r\n                  <div>Category: {{shoe.category.categoryName}}</div>\r\n                  <div>Price: {{shoe.price}}</div>\r\n                  <div>Brand: {{shoe.brand.brandName}}</div>\r\n                  <div><img width=\"100\" height=\"100\" src=\"{{shoe.imageURL}}\"></div>\r\n              </div>\r\n              <div>\r\n                      <span><button type=\"button\" class=\"btn btn-danger btn-sm\" (click)=\"deleteShoe(shoe.id)\">Delete</button></span>\r\n                      <span><button type=\"button\" class=\"btn btn-info btn-sm\" (click)=\"findShoeById(shoe.id)\">Update</button></span>\r\n              </div>\r\n      </li>\r\n  </ul>  \r\n</div>\r\n\r\n\r\n<div style=\"float:right; background-color:gold; width:400px; \">\r\n  \r\n\r\n\r\n  <div title=\"FORMA ZA UNOS NOVIH CIPELA\">\r\n      <div>Add new shoes</div>\r\n      <form (ngSubmit)=\"save()\">\r\n          <div>\r\n              <label for=\"title\">Title:</label>\r\n              <input name=\"title\" [(ngModel)]=\"newShoe.title\"/>\r\n          </div>\r\n          <div>\r\n              <label for=\"price\">Price:</label>\r\n              <input name=\"price\" [(ngModel)]=\"newShoe.price\"/>\r\n          </div>\r\n          <div>\r\n              <label for=\"category\">Category:</label>\r\n              <input name=\"category\" [(ngModel)]=\"newShoe.category.categoryName\"/>\r\n          </div>\r\n          <div>\r\n              <label for=\"brand\">Brand:</label>\r\n              <input name=\"brand\" [(ngModel)]=\"newShoe.brand.brandName\"/>\r\n          </div>\r\n          <div>\r\n                  <label for=\"image\">ImageURL:</label>\r\n                  <input name=\"image\" [(ngModel)]=\"newShoe.imageURL\"/>\r\n              </div>\r\n  \r\n          <input type=\"submit\" value=\"Save\" />\r\n      </form>\r\n  </div>\r\n\r\n  <br><br>\r\n\r\n  <div title=\"FORMA ZA UPDATE CIPELA\">\r\n      <div *ngIf=\"clickedUpdate\">\r\n          <div>Update info</div>\r\n              <form (ngSubmit)=\"updateInfo()\">\r\n                  <div>\r\n                      <label for=\"title\">Change title:</label>\r\n                      <input name=\"title\" [(ngModel)]=\"newShoeUpdate.title\"/>\r\n                  </div>\r\n                  <div>\r\n                      <label for=\"price\">Change price:</label>\r\n                      <input name=\"price\" [(ngModel)]=\"newShoeUpdate.price\"/>\r\n                  </div>\r\n                  <div>\r\n                      <label for=\"category\">Change category:</label>\r\n                      <input name=\"category\" [(ngModel)]=\"newShoeUpdate.category.categoryName\"/>\r\n                  </div><br>\r\n                  <input type=\"submit\" value=\"Save Updates\" />\r\n              </form>\r\n      </div>\r\n  </div>\r\n\r\n  <br><br>\r\n\r\n\r\n  <div title=\"SORTIRANJE I FILTRIRANJE PO CENI\">\r\n     \r\n      <button type=\"button\" style=\"background-color:crimson\" (click)=\"loadData(orderTypes.desc)\">SortByPriceDESC</button>\r\n      <button type=\"button\" style=\"background-color:crimson\" (click)=\"loadData()\">SortByPriceASC</button>\r\n\r\n      <br><br>\r\n\r\n      <form (ngSubmit)=\"filterByPrice()\">\r\n              <div>Insert min price: </div>\r\n              <span><input name=\"price\" type=\"text\" class=\"form-control\" [(ngModel)]=\"priceFilter.lowest\" /></span>\r\n              <div>Insert max price: </div>\r\n              <span><input name=\"price\" type=\"text\" class=\"form-control\" [(ngModel)]=\"priceFilter.highest\" /></span><br>\r\n              <input type=\"submit\" value=\"Filter by price\" />\r\n      </form>\r\n      <br>\r\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetFilter()\">Reset Filter</button>\r\n\r\n  </div>\r\n\r\n</div>   \r\n\r\n<div title=\"LISTE BRENDOVA I KATEGORIJA\" style=\"float:right; background-color: darkcyan; width:300px; margin-left: 10px;margin-right: 40px;\">\r\n\r\n      <p style=\"color: blue ;font-size: larger; font-weight: bold\">List of brands:</p>\r\n          <ul class=\"list-group\">\r\n              <li class=\"list-group-item list-group-item-success\" *ngFor=\"let brand of brands\">\r\n                   <div>Title: {{brand.brandName}}</div>\r\n              </li>\r\n          </ul>\r\n  \r\n      <br><br>\r\n\r\n      <p style=\"color: blue ;font-size: larger; font-weight: bold\">List of categories:</p>\r\n          <ul>\r\n              <li class=\"list-group-item list-group-item-action list-group-item-secondary\" *ngFor=\"let cat of categories\">\r\n                  <div>Title: {{cat.categoryName}}</div> \r\n              </li>\r\n          </ul>\r\n  \r\n</div>\r\n"

/***/ }),

/***/ "./src/app/main-component/main-component.component.ts":
/*!************************************************************!*\
  !*** ./src/app/main-component/main-component.component.ts ***!
  \************************************************************/
/*! exports provided: MainComponentComponent, Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponentComponent", function() { return MainComponentComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _modeliEntiteta_shoe_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modeliEntiteta/shoe.model */ "./src/app/modeliEntiteta/shoe.model.ts");
/* harmony import */ var _modeliEntiteta_category_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modeliEntiteta/category.model */ "./src/app/modeliEntiteta/category.model.ts");
/* harmony import */ var _modeliEntiteta_brand_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../modeliEntiteta/brand.model */ "./src/app/modeliEntiteta/brand.model.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MainComponentComponent = /** @class */ (function () {
    /*
    public brandExists:boolean=false;
    public categoryExists:boolean=false
   */
    function MainComponentComponent(http, httpClient, route) {
        this.http = http;
        this.httpClient = httpClient;
        this.route = route;
        this.shoes = [];
        this.categories = [];
        this.brands = [];
        this.clickedUpdate = false;
        this.orderTypes = Order;
        //this.initData();
        this.priceFilter = {
            lowest: 0,
            highest: 0
        };
        this.newShoe = emptyShoe();
        this.newShoeUpdate = emptyShoe();
        this.loadData();
    }
    MainComponentComponent.prototype.ngOnInit = function () {
    };
    MainComponentComponent.prototype.resetNewShoe = function () {
        this.newShoe = emptyShoe();
    };
    //METODE ZA KOMUNIKACIJU SA SERVEROM:
    MainComponentComponent.prototype.loadData = function (order) {
        var _this = this;
        var params = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["URLSearchParams"]();
        params.set('lowestPrice', this.priceFilter.lowest.toString());
        params.set('highestPrice', this.priceFilter.highest.toString());
        if (order !== undefined) {
            params.set('ord', order.toString());
        }
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ search: params });
        this.http.get('/api/shoes', options).subscribe(function (res) {
            _this.shoes = res.json();
        });
        this.loadBrands();
        this.loadCategories();
    };
    MainComponentComponent.prototype.save = function () {
        var _this = this;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        this.httpClient.post('/api/shoes', JSON.stringify(this.newShoe), { headers: headers }).subscribe(function (resp) {
            _this.loadData();
            _this.resetNewShoe();
        });
    };
    ;
    MainComponentComponent.prototype.deleteShoe = function (id) {
        var _this = this;
        this.http.delete('/api/shoe/' + id).subscribe(function (res) {
            _this.loadData();
        });
    };
    MainComponentComponent.prototype.findShoeById = function (id) {
        var _this = this;
        this.http.get('/api/shoes/' + id).subscribe(function (res) {
            _this.newShoeUpdate = res.json();
            _this.clickedUpdate = true;
        });
    };
    MainComponentComponent.prototype.updateInfo = function () {
        var _this = this;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
        this.http.put("api/shoe/" + this.newShoeUpdate.id, JSON.stringify(this.newShoeUpdate), options).subscribe(function (res) {
            _this.clickedUpdate = false;
            _this.newShoeUpdate = emptyShoe();
            _this.loadData();
        });
    };
    MainComponentComponent.prototype.filterByPrice = function () {
        this.loadData();
    };
    MainComponentComponent.prototype.resetFilter = function () {
        this.priceFilter = {
            lowest: 0,
            highest: 0
        };
        this.loadData();
    };
    MainComponentComponent.prototype.loadBrands = function () {
        var _this = this;
        this.http.get('/api/brands').subscribe(function (res) {
            _this.brands = res.json();
        });
    };
    MainComponentComponent.prototype.loadCategories = function () {
        var _this = this;
        this.http.get('/api/categories').subscribe(function (res) {
            _this.categories = res.json();
        });
    };
    MainComponentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-component',
            template: __webpack_require__(/*! ./main-component.component.html */ "./src/app/main-component/main-component.component.html"),
            styles: [__webpack_require__(/*! ./main-component.component.css */ "./src/app/main-component/main-component.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], MainComponentComponent);
    return MainComponentComponent;
}()); //end class

var Order;
(function (Order) {
    Order[Order["asc"] = 0] = "asc";
    Order[Order["desc"] = 1] = "desc";
})(Order || (Order = {}));
var emptyShoe = function () {
    return new _modeliEntiteta_shoe_model__WEBPACK_IMPORTED_MODULE_1__["Shoe"]({
        id: 0,
        title: '',
        imageURL: '',
        price: 0,
        category: new _modeliEntiteta_category_model__WEBPACK_IMPORTED_MODULE_2__["Category"]({
            id: 0,
            categoryName: ''
        }),
        brand: new _modeliEntiteta_brand_model__WEBPACK_IMPORTED_MODULE_3__["Brand"]({
            id: 0,
            brandName: ''
        })
    });
};
var emptyCategory = function () {
    return new _modeliEntiteta_category_model__WEBPACK_IMPORTED_MODULE_2__["Category"]({
        id: 0,
        categoryName: ''
    });
};
var emptyBrand = function () {
    return new _modeliEntiteta_brand_model__WEBPACK_IMPORTED_MODULE_3__["Brand"]({
        id: 0,
        brandName: ''
    });
};


/***/ }),

/***/ "./src/app/modeliEntiteta/brand.model.ts":
/*!***********************************************!*\
  !*** ./src/app/modeliEntiteta/brand.model.ts ***!
  \***********************************************/
/*! exports provided: Brand */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Brand", function() { return Brand; });
var Brand = /** @class */ (function () {
    function Brand(brandCfg) {
        this.id = brandCfg.id;
        this.brandName = brandCfg.brandName;
    }
    return Brand;
}());



/***/ }),

/***/ "./src/app/modeliEntiteta/category.model.ts":
/*!**************************************************!*\
  !*** ./src/app/modeliEntiteta/category.model.ts ***!
  \**************************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
var Category = /** @class */ (function () {
    function Category(categoryCfg) {
        this.id = categoryCfg.id;
        this.categoryName = categoryCfg.categoryName;
    }
    return Category;
}());



/***/ }),

/***/ "./src/app/modeliEntiteta/shoe.model.ts":
/*!**********************************************!*\
  !*** ./src/app/modeliEntiteta/shoe.model.ts ***!
  \**********************************************/
/*! exports provided: Shoe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Shoe", function() { return Shoe; });
var Shoe = /** @class */ (function () {
    function Shoe(shoeCfg) {
        this.id = shoeCfg.id;
        this.title = shoeCfg.title;
        this.imageURL = shoeCfg.imageURL;
        this.price = shoeCfg.price;
        this.category = shoeCfg.category;
        this.brand = shoeCfg.brand;
    }
    return Shoe;
}());



/***/ }),

/***/ "./src/app/page-not-found-component/page-not-found-component.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/page-not-found-component/page-not-found-component.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/page-not-found-component/page-not-found-component.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/page-not-found-component/page-not-found-component.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>\r\n    There is nothing here!\r\n</h1> \r\n\r\n"

/***/ }),

/***/ "./src/app/page-not-found-component/page-not-found-component.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/page-not-found-component/page-not-found-component.component.ts ***!
  \********************************************************************************/
/*! exports provided: PageNotFoundComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponentComponent", function() { return PageNotFoundComponentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponentComponent = /** @class */ (function () {
    function PageNotFoundComponentComponent() {
    }
    PageNotFoundComponentComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-not-found-component',
            template: __webpack_require__(/*! ./page-not-found-component.component.html */ "./src/app/page-not-found-component/page-not-found-component.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found-component.component.css */ "./src/app/page-not-found-component/page-not-found-component.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponentComponent);
    return PageNotFoundComponentComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\OBUKA\SHOESTORE\projekat\ShueStore\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map