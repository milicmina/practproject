package shop.shoe.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.shoe.model.Category;
import shop.shoe.service.CategoryService;





@RestController
public class CategoryController {

	@Autowired
    private CategoryService categoryService;
	
	
	
	@GetMapping(value = "api/categories")
    public ResponseEntity<List<Category>> getAll() {
        final List<Category> retVal = categoryService.findAll();

        return new ResponseEntity<>(retVal, HttpStatus.OK);
    }
	
	
	@GetMapping(value = "api/categories/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable Long id) {
        Category category = categoryService.findOne(id);

        if (category == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(category, HttpStatus.OK);
    }
	
	
	@RequestMapping(value = "api/categories", method = RequestMethod.GET, params = "categoryName")
	public ResponseEntity<Category> getCategoryByName(@RequestParam String categoryName) {
		
		Category category= categoryService.findByName(categoryName);
		
		return new ResponseEntity<>(category, HttpStatus.OK);
	}

	
	
}//end class
