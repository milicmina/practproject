package shop.shoe.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.shoe.model.Brand;
import shop.shoe.service.BrandService;




@RestController
public class BrandController {

	
	@Autowired
    private BrandService brandService;
	
	
	
	@GetMapping(value = "api/brands")
    public ResponseEntity<List<Brand>> getAll() {
        final List<Brand> retVal = brandService.findAll();

        return new ResponseEntity<>(retVal, HttpStatus.OK);
    }
	
	
	@GetMapping(value = "api/brands/{id}")
    public ResponseEntity<Brand> getBrand(@PathVariable Long id) {
		
        Brand brand = brandService.findOne(id);

        if (brand == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(brand, HttpStatus.OK);
    }

	
	@GetMapping(value = "api/brands", params = "brandName")
	public ResponseEntity<Brand> getBrandByName(@RequestParam String brandName) {
		
		Brand brand=brandService.findByName(brandName);
		
		return new ResponseEntity<>(brand, HttpStatus.OK);
	}
	
	
	
}//end class
