package shop.shoe.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import shop.shoe.model.Brand;
import shop.shoe.model.Shoe;
import shop.shoe.service.ShoeService;







@RestController
public class ShoeController {

	@Autowired
    private ShoeService shoeService;
	
	
	
	@GetMapping(value = "api/shoes")
    public ResponseEntity<List<Shoe>> getAll(@RequestParam(required=false, defaultValue="0") int ord,
			@RequestParam(required=false, defaultValue="0") int lowestPrice,
			@RequestParam(required=false, defaultValue="0") int highestPrice) {
		
		List<Shoe> shoes;
		if (ord==0) {
			if(lowestPrice != 0 || highestPrice != 0){
				shoes = shoeService.findAllOrderByPriceAsc(lowestPrice,highestPrice);							
			}
			else{
				shoes = shoeService.findAllOrderByPriceAsc();			
			}
		}
		else{
			if(lowestPrice != 0 || highestPrice != 0){
				shoes = shoeService.findAllOrderByPriceDesc(lowestPrice,highestPrice);							
			}
			else{
				shoes = shoeService.findAllOrderByPriceDesc();			
			}
		}
		return new ResponseEntity<>(shoes, HttpStatus.OK); 
    }
	
	
	
	
	@GetMapping(value = "api/shoes/{id}")
    public ResponseEntity<Shoe> getShoe(@PathVariable Long id) {
		
        Shoe shoe = shoeService.findOne(id);

        if (shoe == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(shoe, HttpStatus.OK);
    }
	
	
	
	@PostMapping(value = "api/shoes")
    public ResponseEntity<Shoe> create(@RequestBody Shoe shoe) {
        final Shoe savedShoe = shoeService.save(shoe);
        return new ResponseEntity<>(savedShoe, HttpStatus.CREATED);
    }
	
    
	
	
   

    
    
    @PutMapping(value = "api/shoe/{id}")
    public ResponseEntity<Shoe> update(	@PathVariable Long id, @RequestBody Shoe shoe) {
    	
        Shoe foundShoe = shoeService.findOne(id);
        if (shoe == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
  
        foundShoe.setTitle(shoe.getTitle());
        foundShoe.setPrice(shoe.getPrice());
        foundShoe.setImageURL(shoe.getImageURL());
        foundShoe.setCategory(shoe.getCategory());
        foundShoe.setBrand(shoe.getBrand());
        
        Shoe savedShoe = shoeService.save(foundShoe);
        return new ResponseEntity<>(savedShoe, HttpStatus.CREATED);
    }
    
    
    
    @SuppressWarnings("rawtypes")
	@DeleteMapping(value = "api/shoe/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
    	
        final Shoe shoe = shoeService.findOne(id);
        if (shoe == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        shoeService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

	
	
	
}//end class
