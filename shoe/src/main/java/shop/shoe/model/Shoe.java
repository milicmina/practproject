package shop.shoe.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;





@Entity
public class Shoe {

	
	@Id
	@GeneratedValue
	private Long id;

	private String title;
	
	private double price;
	
	private String imageURL;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) 
	private Category category;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) 
	private Brand brand;

	
	
	public Shoe() {
		super();
	}


	public Shoe(Long id, String title, double price, String imageURL, Category category, Brand brand) {
		super();
		this.id = id;
		this.title = title;
		this.price = price;
		this.imageURL = imageURL;
		this.category = category;
		this.brand = brand;
	}


	

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getImageURL() {
		return imageURL;
	}



	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}



	public Category getCategory() {
		return category;
	}



	public void setCategory(Category category) {
		this.category = category;
	}



	public Brand getBrand() {
		return brand;
	}



	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Shoe other = (Shoe) obj;
		return id == other.id;
	}
	

	
}//end class
