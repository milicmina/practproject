package shop.shoe.util;

public enum SortingOrder {
    ASC,
    DESC
}
