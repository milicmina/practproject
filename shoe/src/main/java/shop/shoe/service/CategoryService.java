package shop.shoe.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.shoe.model.Category;
import shop.shoe.repository.CategoryRepository;




@Component
public class CategoryService {

	@Autowired 
	private CategoryRepository categoryRepository;
	
	
	
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	public Category findOne(Long id) {
		return categoryRepository.findOne(id);
	}
	
	public Category findByName(String name) {
		return categoryRepository.findByCategoryNameContains(name);
	}
	
}//end class
