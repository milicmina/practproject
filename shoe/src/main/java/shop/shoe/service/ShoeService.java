package shop.shoe.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import shop.shoe.model.Brand;
import shop.shoe.model.Category;
import shop.shoe.model.Shoe;
import shop.shoe.repository.BrandRepository;
import shop.shoe.repository.CategoryRepository;
import shop.shoe.repository.ShoeRepository;






@Component
public class ShoeService {
	
	@Autowired 
	private ShoeRepository shoeRepository;
	
	@Autowired
	private BrandRepository brandRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public List<Shoe> findAll() {
		return shoeRepository.findAll();
	}

	public Shoe findOne(Long id) {
		return shoeRepository.findOne(id);
	}
	
	public Shoe save(Shoe shoe) {
		List<Brand> brands=brandRepository.findAll();
		for (int i = 0; i < brands.size(); i++) {
			if (shoe.getBrand().getBrandName().equalsIgnoreCase(brands.get(i).getBrandName())) {
				shoe.setBrand(brands.get(i));
			}
		}
		
		List<Category> categories=categoryRepository.findAll();
		for (int i = 0; i < categories.size(); i++) {
			if (shoe.getCategory().getCategoryName().equalsIgnoreCase(categories.get(i).getCategoryName())) {
				shoe.setCategory(categories.get(i));
			}
		}
		
		return shoeRepository.save(shoe);
	}


	public void delete(Long id) {
		shoeRepository.delete(id);
	}
	
	
	
	public List<Shoe> findAllOrderByPriceAsc(){
		return shoeRepository.findAllByOrderByPriceAsc();
	}

	public List<Shoe> findAllOrderByPriceDesc(){
		return shoeRepository.findAllByOrderByPriceDesc();
	}
	
	public List<Shoe> findAllOrderByPriceAsc(double lowestPrice, double higestPrice){
		return shoeRepository.findByPriceBetweenOrderByPriceAsc(lowestPrice, higestPrice);
	}

	public List<Shoe> findAllOrderByPriceDesc(double lowestPrice, double higestPrice){
		return shoeRepository.findByPriceBetweenOrderByPriceDesc(lowestPrice, higestPrice);
	}
	
	
	
}//end class
