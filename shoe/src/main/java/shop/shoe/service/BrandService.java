package shop.shoe.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.shoe.model.Brand;
import shop.shoe.repository.BrandRepository;



@Component
public class BrandService {

	@Autowired 
	private BrandRepository brandRepository;
	
	
	
	
	public List<Brand> findAll() {
		return brandRepository.findAll();
	}
	
	public Brand findOne(Long id) {
		return brandRepository.findOne(id);
	}
	
	public Brand findByName(String brandName) {
		return brandRepository.findByBrandName(brandName);
	}
	
}//end class
