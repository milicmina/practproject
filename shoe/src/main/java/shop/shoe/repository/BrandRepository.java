package shop.shoe.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import shop.shoe.model.Brand;




@Component
public interface BrandRepository extends JpaRepository<Brand, Long> {

	public Brand findByBrandName(String brandName);
	
	
	
}//end class
