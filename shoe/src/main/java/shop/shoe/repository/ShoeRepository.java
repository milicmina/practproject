package shop.shoe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import shop.shoe.model.Shoe;




@Component
public interface ShoeRepository extends JpaRepository<Shoe, Long>{
	
	
	
	List<Shoe> findAllByOrderByPriceAsc();
	List<Shoe> findAllByOrderByPriceDesc();
	List<Shoe> findByPriceBetweenOrderByPriceAsc(double lowestPrice, double higestPrice);
	List<Shoe> findByPriceBetweenOrderByPriceDesc(double lowestPrice, double higestPrice);
	
	
	
	
	
}//end class
