package shop.shoe.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import shop.shoe.model.Category;




@Component
public interface CategoryRepository extends JpaRepository<Category, Long>{

	
	public Category findByCategoryNameContains(String name);
	
	
	
	
	
	
}//end class
